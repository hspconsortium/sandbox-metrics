package org.hspconsortium.sandbox.sandboxmetrics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SandboxMetricsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SandboxMetricsApplication.class, args);
	}
}
